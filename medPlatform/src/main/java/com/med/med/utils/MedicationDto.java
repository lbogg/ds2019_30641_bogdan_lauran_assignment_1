package com.med.med.utils;

public class MedicationDto {
    private long userId;
    private String name;
    private int dosage;
    private String periodStart;
    private String periodEnd;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public String getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(String period) {
        this.periodStart = period;
    }

    public String getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(String periodEnd) {
        this.periodEnd = periodEnd;
    }
}
