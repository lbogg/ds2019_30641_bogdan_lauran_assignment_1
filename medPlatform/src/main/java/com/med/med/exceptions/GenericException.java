package com.med.med.exceptions;

public class GenericException extends RuntimeException {
    public GenericException(String msg) {super(msg);}
}
