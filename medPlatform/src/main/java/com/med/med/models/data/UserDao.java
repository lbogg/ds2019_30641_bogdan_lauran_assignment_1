package com.med.med.models.data;

import com.med.med.models.User;
import com.med.med.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface UserDao extends JpaRepository<User, Long> {
    List<User> findAllByRole(Role role);
    Optional<User> findByUsername(String username);
}
