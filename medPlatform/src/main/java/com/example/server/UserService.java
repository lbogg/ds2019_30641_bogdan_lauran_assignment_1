package com.example.server;

import com.example.grpc.*;
import com.med.med.models.Medication;
import com.med.med.models.data.MedicationDao;
import com.med.med.services.MedService;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


public class UserService extends UserServiceGrpc.UserServiceImplBase {

    MedRepo medRepo = new MedRepo();

    @Override
    public void planDisplay(DisplayRequest request, StreamObserver<DisplayResponse> responseObserver) {
        System.out.println("Displaying");

        DisplayResponse.Builder response = DisplayResponse.newBuilder();
        MedGrpc.Builder medGrpc = MedGrpc.newBuilder();
        String greeting = request.getGreeting();

        if(greeting.equals("get")){
            List<Medication> medications = medRepo.getMedications(1);
            for(Medication  m: medications){
                medGrpc.setMedPlanId((int) m.getId());
                medGrpc.setStart(m.getPeriodStart());
                medGrpc.setEnd(m.getPeriodEnd());
                medGrpc.setName(m.getName());
                response.addMedPlanList(medGrpc);
            }
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void notice(MedStatus request, StreamObserver<StatusResponse> responseObserver) {

        StatusResponse.Builder status = StatusResponse.newBuilder();

        if(request.getStatus().equals("taken")){
            status.setRsp("Medicine taken");
        }
        else{ status.setRsp("Medicine not taken"); }

        responseObserver.onNext(status.build());
        responseObserver.onCompleted();
    }
}
