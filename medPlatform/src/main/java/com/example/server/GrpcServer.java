package com.example.server;

import com.med.med.models.data.MedicationDao;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class GrpcServer {

    @Autowired
    MedicationDao medicationDao;

    public static void main(String[] args) throws IOException, InterruptedException {

        Server server = ServerBuilder.forPort(8080).addService(new UserService()).build();

        server.start();
        System.out.println("Server started on port " + server.getPort());
        server.awaitTermination();
    }
}
