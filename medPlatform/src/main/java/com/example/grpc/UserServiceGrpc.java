package com.example.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: User.proto")
public final class UserServiceGrpc {

  private UserServiceGrpc() {}

  public static final String SERVICE_NAME = "com.example.grpc.UserService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.grpc.MedStatus,
      com.example.grpc.StatusResponse> getNoticeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "notice",
      requestType = com.example.grpc.MedStatus.class,
      responseType = com.example.grpc.StatusResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.MedStatus,
      com.example.grpc.StatusResponse> getNoticeMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.MedStatus, com.example.grpc.StatusResponse> getNoticeMethod;
    if ((getNoticeMethod = UserServiceGrpc.getNoticeMethod) == null) {
      synchronized (UserServiceGrpc.class) {
        if ((getNoticeMethod = UserServiceGrpc.getNoticeMethod) == null) {
          UserServiceGrpc.getNoticeMethod = getNoticeMethod = 
              io.grpc.MethodDescriptor.<com.example.grpc.MedStatus, com.example.grpc.StatusResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "com.example.grpc.UserService", "notice"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.MedStatus.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.StatusResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new UserServiceMethodDescriptorSupplier("notice"))
                  .build();
          }
        }
     }
     return getNoticeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.DisplayRequest,
      com.example.grpc.DisplayResponse> getPlanDisplayMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "planDisplay",
      requestType = com.example.grpc.DisplayRequest.class,
      responseType = com.example.grpc.DisplayResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.DisplayRequest,
      com.example.grpc.DisplayResponse> getPlanDisplayMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.DisplayRequest, com.example.grpc.DisplayResponse> getPlanDisplayMethod;
    if ((getPlanDisplayMethod = UserServiceGrpc.getPlanDisplayMethod) == null) {
      synchronized (UserServiceGrpc.class) {
        if ((getPlanDisplayMethod = UserServiceGrpc.getPlanDisplayMethod) == null) {
          UserServiceGrpc.getPlanDisplayMethod = getPlanDisplayMethod = 
              io.grpc.MethodDescriptor.<com.example.grpc.DisplayRequest, com.example.grpc.DisplayResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "com.example.grpc.UserService", "planDisplay"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.DisplayRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.DisplayResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new UserServiceMethodDescriptorSupplier("planDisplay"))
                  .build();
          }
        }
     }
     return getPlanDisplayMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static UserServiceStub newStub(io.grpc.Channel channel) {
    return new UserServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static UserServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new UserServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static UserServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new UserServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class UserServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void notice(com.example.grpc.MedStatus request,
        io.grpc.stub.StreamObserver<com.example.grpc.StatusResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getNoticeMethod(), responseObserver);
    }

    /**
     */
    public void planDisplay(com.example.grpc.DisplayRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.DisplayResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getPlanDisplayMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getNoticeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.MedStatus,
                com.example.grpc.StatusResponse>(
                  this, METHODID_NOTICE)))
          .addMethod(
            getPlanDisplayMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.DisplayRequest,
                com.example.grpc.DisplayResponse>(
                  this, METHODID_PLAN_DISPLAY)))
          .build();
    }
  }

  /**
   */
  public static final class UserServiceStub extends io.grpc.stub.AbstractStub<UserServiceStub> {
    private UserServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private UserServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UserServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new UserServiceStub(channel, callOptions);
    }

    /**
     */
    public void notice(com.example.grpc.MedStatus request,
        io.grpc.stub.StreamObserver<com.example.grpc.StatusResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNoticeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void planDisplay(com.example.grpc.DisplayRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.DisplayResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPlanDisplayMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class UserServiceBlockingStub extends io.grpc.stub.AbstractStub<UserServiceBlockingStub> {
    private UserServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private UserServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UserServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new UserServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.example.grpc.StatusResponse notice(com.example.grpc.MedStatus request) {
      return blockingUnaryCall(
          getChannel(), getNoticeMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.DisplayResponse planDisplay(com.example.grpc.DisplayRequest request) {
      return blockingUnaryCall(
          getChannel(), getPlanDisplayMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class UserServiceFutureStub extends io.grpc.stub.AbstractStub<UserServiceFutureStub> {
    private UserServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private UserServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UserServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new UserServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.StatusResponse> notice(
        com.example.grpc.MedStatus request) {
      return futureUnaryCall(
          getChannel().newCall(getNoticeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.DisplayResponse> planDisplay(
        com.example.grpc.DisplayRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getPlanDisplayMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_NOTICE = 0;
  private static final int METHODID_PLAN_DISPLAY = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final UserServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(UserServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_NOTICE:
          serviceImpl.notice((com.example.grpc.MedStatus) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.StatusResponse>) responseObserver);
          break;
        case METHODID_PLAN_DISPLAY:
          serviceImpl.planDisplay((com.example.grpc.DisplayRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.DisplayResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class UserServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    UserServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.grpc.User.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("UserService");
    }
  }

  private static final class UserServiceFileDescriptorSupplier
      extends UserServiceBaseDescriptorSupplier {
    UserServiceFileDescriptorSupplier() {}
  }

  private static final class UserServiceMethodDescriptorSupplier
      extends UserServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    UserServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (UserServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new UserServiceFileDescriptorSupplier())
              .addMethod(getNoticeMethod())
              .addMethod(getPlanDisplayMethod())
              .build();
        }
      }
    }
    return result;
  }
}
